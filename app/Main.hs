{-# LANGUAGE OverloadedStrings #-}
module Main (main) where

import System.IO
import Terminal.Terminal
import Terminal.Network

main :: IO ()
main = hSetBuffering stdout NoBuffering >> timespec >> hello >> loop
    where loop = do prompt
                    ans <- getAnswerC
                    case ans of
                        Just 'h' -> help >> loop
                        Just 'v' -> version >> loop
                        Just 'p' -> profilePrompt >> loop
                        Just 'n' -> genesisPrompt >> loop
                        Just 'd' -> deletionPrompt >> loop
                        Just 'a' -> newTransactionPrompt >> loop
                        Just 'c' -> collapseDisplay >> loop
                        Just 'j' -> blocksJSONPrompt >> loop
                        Just 'q' -> return ()
                        _ -> idk (fmap pure ans) >> loop

