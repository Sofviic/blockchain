module Profiles.Keys where

import qualified Data.ByteString.Char8 as B
import System.Directory
import Crypto.Hash
import Crypto.PubKey.RSA
import Crypto.PubKey.RSA.PKCS15
import Types
import Profiles.Utils
import Utils.FilePaths
import Utils.Maybe

generateProfile :: Username -> IO ()
generateProfile name = do
                        let dir = profilesDir <> name <> "/"
                        createDirectory dir
                        (publicKey, privateKey) <- generate 512 0x10001
                        writeFile (dir <> "id_rsa") . show $ privateKey
                        writeFile (dir <> "id_rsa.pub") . show $ publicKey
                        addContactWithKey name publicKey

addContactWithFile :: Username -> FilePath -> IO ()
addContactWithFile name keyfile = read <$> readFile keyfile >>= addContactWithKey name

addContact :: Username -> Integer -> IO ()
addContact name publicN = addContactWithKey name $ PublicKey 512 publicN 0x10001

addContactWithKey :: Username -> PublicKey -> IO ()
addContactWithKey name key = createDirectoryIfMissing True contactsDir
                            >> createDirectory (contactsDir <> name <> "/")
                            >> (writeFile (contactsDir <> name <> "/" <> "id_rsa.pub") . show) key

signWithProfile :: Username -> FilePath -> FilePath -> IO ()
signWithProfile name file dest = do
                                let dir = profilesDir <> name <> "/"
                                key <- fmap read . readFile $ dir <> "id_rsa"
                                msg <- B.readFile file
                                case sign Nothing (Just SHA512) key msg of
                                    Left err -> putStrLn . ("Error!: "<>) . show $ err
                                    Right s -> B.writeFile dest s

signBSWithProfile :: Username -> B.ByteString -> IO B.ByteString
signBSWithProfile name bs = do
                            ms <- getID name
                            case ms of
                                Nothing -> error $ "Error: Profile " <> name <> " does not exist."
                                Just _ -> do
                                    let dir = profilesDir <> name <> "/"
                                    key <- fmap read . readFile $ dir <> "id_rsa"
                                    case sign Nothing (Just SHA512) key bs of
                                            Left err -> error . ("Error!: "<>) . show $ err
                                            Right s -> return s

signBSWithProfileMaybe :: Username -> B.ByteString -> IO (Maybe B.ByteString)
signBSWithProfileMaybe name bs = do
                            ms <- getID name
                            case ms of
                                Nothing -> return Nothing
                                Just _ -> do
                                    let dir = profilesDir <> name <> "/"
                                    key <- fmap read . readFile $ dir <> "id_rsa"
                                    return $ rightToMaybe $ sign Nothing (Just SHA512) key bs

verifyWithKey :: FilePath -> FilePath -> FilePath -> IO Bool
verifyWithKey keyfile file signature = do
                                    key <- read <$> readFile keyfile
                                    msg <- B.readFile file
                                    sgn <- B.readFile signature
                                    return $ verify (Just SHA512) key msg sgn

