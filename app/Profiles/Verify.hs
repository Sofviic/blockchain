module Profiles.Verify where

import BlockChain.Block
import BlockChain.Types
import Profiles.Utils
import Types

isValidProfileName :: String -> Bool
isValidProfileName = liftA2 (&&) ((==4) . length) $ all (`elem` ['A'..'Z'])

verifyKey :: UserID -> Block -> Signature -> IO Bool
verifyKey uid b sign = do
                        mcun <- getContactUsername uid
                        case mcun of
                            Nothing -> putStrLn "No matching profile." >> return False
                            Just cuname -> do
                                mb <- signBlockMaybe cuname b
                                case mb of
                                    Nothing -> putStrLn "No matching profile." >> return False
                                    Just sb -> if senderSign sb == Just sign 
                                        then return True 
                                        else putStrLn "False signature." >> return False
