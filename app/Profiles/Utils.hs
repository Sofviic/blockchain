module Profiles.Utils where

import System.Directory(doesDirectoryExist)
import Types
import Utils.FilePaths
import Utils.Intify

getID,getContactID :: Username -> IO (Maybe UserID)
getID = getIDin profilesDir
getContactID = getIDin contactsDir

getIDin :: FilePath -> Username -> IO (Maybe UserID)
getIDin dir name = doesDirectoryExist (dir <> name) >>= \b -> 
                    if b
                    then return . Just  $ int64ify name
                    else return Nothing

getUsername,getContactUsername :: UserID -> IO (Maybe Username)
getUsername = findUsernameIn profilesDir
getContactUsername = findUsernameIn contactsDir

findUsernameIn :: FilePath -> UserID -> IO (Maybe Username)
findUsernameIn dir uid = doesDirectoryExist (dir <> unint64ify uid) >>= \b -> 
                    if b
                    then return . Just  $ unint64ify uid
                    else return Nothing
