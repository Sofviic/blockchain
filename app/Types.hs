module Types where

import Data.Int(Int64)

type Username = String
type UserID = Int64

