module BlockChain.BlockChain where

import qualified Data.ByteString.Lazy as B
import Data.Maybe(listToMaybe)
import Data.List(sortOn)
import Control.Monad(forM)
import System.Directory(listDirectory)
import Text.Regex.TDFA((=~))
import BlockChain.Block
import BlockChain.Serialise
import BlockChain.Types
import Profiles.Utils
import Utils.List
import Types

genesis :: BlockChain
genesis = []

createNewTransaction :: Username -> Username -> Amount -> String -> BlockChain -> IO (Maybe BlockChain)
createNewTransaction sender recipient amount info bc
    = do
        ms <- getID sender
        mr <- getContactID recipient
        case ms of
            Nothing -> putStrLn ("Error: Profile " <> sender <> " does not exist.") >> return Nothing
            Just s -> case mr of
                Nothing -> putStrLn ("Error: Contact " <> recipient <> " does not exist.") >> return Nothing
                Just r -> createNewBlock (listToMaybe bc) s r amount info
                        >>= signBlock sender
                        >>= return . Just . (:bc) . finaliseBlock

saveBlockChain :: FilePath -> BlockChain -> IO ()
saveBlockChain dir = mapM_ $ \b -> B.writeFile (dir <> show (blockNum b) <> ".block") . serialiseBlock $ b

loadBlockChain :: FilePath -> IO BlockChain
loadBlockChain dir = do 
                        bs <- reverse
                            . getSeqFromOn snd 0
                            . sortOn snd 
                            . fmap (\bfp -> (bfp, read . dropLast (length ".block") $ bfp)) 
                            . filter (=~ "^[0-9]+.block$") 
                            <$> listDirectory dir :: IO [(FilePath,Integer)]
                        bc <- forM bs $ \(b,i) -> do
                            bb <- fmap (B.fromStrict . B.toStrict) $ B.readFile $ dir <> b
                            case deserialiseBlock bb of
                                Nothing -> error $ "Error!: Couldn't read block " <> show i
                                Just block -> return block
                        return bc

collapse :: BlockChain -> [(UserID,Amount)]
collapse = foldr (\b -> addTo [(senderID b, negate $ transaction b),(recipientID b, transaction b)]) []

addTo :: [(UserID,Amount)] -> [(UserID,Amount)] -> [(UserID,Amount)]
addTo [] r = r
addTo ((uid,a):ps) r | uid `elem` fmap fst r = addTo ps $ fmap (\(rid,ra) -> (,) rid $ ra + if uid == rid then a else 0) r
addTo (p:ps) r = addTo ps $ p : r
