module BlockChain.Verify where

import Data.ByteString.Lazy as B
--import Data.Int(Int64,Int32)
import Crypto.Hash(SHA512(..),hashWith)
import Profiles.Verify
import BlockChain.Serialise
import BlockChain.Types
import Utils.List(onEveryLink)

hashLinkSounded :: BlockChain -> Bool
hashLinkSounded = onEveryLink $ \cb b -> prevHash b == hashWith SHA512 (toStrict $ serialiseBlock cb)

timeLineSounded :: BlockChain -> Bool
timeLineSounded = onEveryLink $ \cb b -> timestamp b < timestamp cb

signsSounded :: BlockChain -> IO Bool
signsSounded = fmap and . mapM verifyKeyOfBlock

verifyKeyOfBlock :: Block -> IO Bool
verifyKeyOfBlock b = case senderSign b of 
                        Nothing -> return False
                        Just sign -> verifyKey (senderID b) b sign
