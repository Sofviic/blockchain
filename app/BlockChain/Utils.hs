{-# LANGUAGE OverloadedStrings #-}
module BlockChain.Utils where

import qualified Data.ByteString as B
import qualified Data.ByteString.Lazy as BL
import Crypto.Hash(Digest,hash,HashAlgorithm)
import Data.Int(Int64)
import BlockChain.Types

zeros :: BL.ByteString
zeros = BL.replicate maxBlockSize $ fromInteger 0

bszero :: Int64 -> BL.ByteString
bszero n = BL.replicate n $ fromInteger 0
-- 
-- szero :: Int -> String
-- szero n = Prelude.replicate n $ toEnum 0

dzero :: HashAlgorithm a => Digest a
dzero = hash ("" :: B.ByteString)

zeroFields :: Bool -> Bool -> Block -> Block
zeroFields signF hashF (Block ph bnum vnum time s sign r amount info h)
    = Block ph bnum vnum time s (if signF then Nothing else sign) r amount info (if hashF then Nothing else h)

zeroNonSignFields, zeroNonHashFields :: Block -> Block
zeroNonSignFields = zeroFields True True
zeroNonHashFields = zeroFields False True
