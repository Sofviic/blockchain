{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE DeriveGeneric #-}
module BlockChain.Types where

import GHC.Generics
import Data.Aeson(ToJSON(..),object,(.=))
import Data.ByteString(ByteString)
import Data.Int(Int64,Int32)
import Crypto.Hash(Digest,SHA512)
import Data.Function(on)
import Utils.Show
import Utils.Iso
import Utils.Enum
import Utils.Bases
import Utils.Maybe
import Types

version :: Int32
version = 1

maxBlockSize :: Num a => a
maxBlockSize = 2048

type Amount = Int64
type Signature = ByteString --only 512 bytes
type Info = String --only 1364 bytes

data Block = Block { -- 2kB in total
    prevHash :: Digest SHA512,
    blockNum :: Int64,
    versionNum :: Int32,
    timestamp :: Int64,
    senderID :: UserID,
    senderSign :: Maybe Signature, --TODO: no longer has to be Maybe
    recipientID :: UserID,
    transaction :: Amount,
    transactionInfo :: Info, 
    bodyHash :: Maybe (Digest SHA512)
    } deriving (Show,Eq,Generic)
instance ToJSON Block where
    toJSON b = object   ["prevHash" .= show (digestpack $ prevHash b)
                        ,"blockNum" .= blockNum b
                        ,"versionNum" .= versionNum b
                        ,"timestamp" .= timestamp b
                        ,"senderID" .= senderID b
                        ,"senderSign" .= fmap show (senderSign b)
                        ,"recipientID" .= recipientID b
                        ,"transaction" .= transaction b
                        ,"transactionInfo" .= transactionInfo b
                        ,"bodyHash" .= (fmap (show . digestpack) $ bodyHash b)]

data MinBlock = MinBlock {
    min_prevHash :: Bool,
    min_blockNum :: Int64,
    min_versionNum :: Int32,
    min_timestamp :: Int64,
    min_senderID :: Username,
    min_senderSign :: Bool,
    min_recipientID :: Username,
    min_transaction :: Double,
    min_transactionInfo :: Info, 
    min_bodyHash :: Bool
    } deriving (Show,Eq,Generic)
instance ToJSON MinBlock where
    toJSON b = object   ["prevHash" .= min_prevHash b
                        ,"blockNum" .= min_blockNum b
                        ,"versionNum" .= min_versionNum b
                        ,"timestamp" .= min_timestamp b
                        ,"senderID" .= min_senderID b
                        ,"senderSign" .= min_senderSign b
                        ,"recipientID" .= min_recipientID b
                        ,"transaction" .= min_transaction b
                        ,"transactionInfo" .= min_transactionInfo b
                        ,"bodyHash" .= min_bodyHash b]

minimiseBlock :: Block -> MinBlock
minimiseBlock (Block _ bn vn ts sid ssign rid a info bh) = 
        MinBlock True bn vn ts (id2Username sid) (maybeBool $ forgetMaybe ssign) (id2Username rid) (cast a / 1000000) (filter(/=toEnum 0) info) (maybeBool $ forgetMaybe bh)

id2Username :: UserID -> Username
id2Username = un0 . reverse . take 8 . fmap cast . baseDigits 256

digestpack :: Digest SHA512 -> String
digestpack = spack

instance Ord Block where
    (<=) = on (<=) blockNum

type BlockChain = [Block]
