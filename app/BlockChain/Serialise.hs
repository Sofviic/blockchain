{-# LANGUAGE OverloadedStrings #-}
module BlockChain.Serialise where

import Prelude as P
import qualified Data.ByteString as SB
import Data.ByteString.Lazy as B
import Data.ByteString.Lazy.Char8 as BC
import Data.Maybe(fromMaybe)
import Data.Int(Int64,Int32)
import Data.Binary(encode)
import BlockChain.Types
import BlockChain.Utils
import Crypto.Hash
import Utils.Show
import Utils.Enum

deserialiseBlock :: ByteString -> Maybe Block
deserialiseBlock s = do
    ph    <- prevHashDS (lbsplice 0 64 s)
    bn    <- blockNumDS (lbsplice (64) 8 s)
    v     <- versionNumDS (lbsplice (64+8) 4 s)
    t     <- timestampDS (lbsplice (64+8+4) 8 s)
    sid   <- senderIDDS (lbsplice (64+8+4+8) 8 s)
    ssign <- senderSignDS (lbsplice (64+8+4+8+8) 512 s)
    rid   <- recipientIDDS (lbsplice (64+8+4+8+8+512) 8 s)
    a     <- transactionDS (lbsplice (64+8+4+8+8+512+8) 8 s)
    info  <- transactionInfoDS (lbsplice (64+8+4+8+8+512+8+8) 1364 s)
    bh    <- bodyHashDS (lbsplice (64+8+4+8+8+512+8+8+1364) 64 s)
    return $ Block ph bn v t sid (Just ssign) rid a info (Just bh)

prevHashDS :: ByteString -> Maybe (Digest SHA512)
prevHashDS = digestFromByteString . toStrict
blockNumDS :: ByteString -> Maybe Int64
blockNumDS = Just . bsint64
versionNumDS :: ByteString -> Maybe Int32
versionNumDS = Just . bsint32
timestampDS :: ByteString -> Maybe Int64
timestampDS = Just . bsint64
senderIDDS :: ByteString -> Maybe Int64
senderIDDS = Just . bsint64
senderSignDS :: ByteString -> Maybe Signature
senderSignDS = Just . toStrict
recipientIDDS :: ByteString -> Maybe Int64
recipientIDDS = Just . bsint64
transactionDS :: ByteString -> Maybe Amount
transactionDS = Just . bsint64
transactionInfoDS :: ByteString -> Maybe Info
transactionInfoDS = Just . BC.unpack
bodyHashDS :: ByteString -> Maybe (Digest SHA512)
bodyHashDS = digestFromByteString . toStrict

bsint32 :: ByteString -> Int32
bsint32 = B.foldl (\a b -> 256 * a + cast b) 0 . bsPad 4
bsint64 :: ByteString -> Int64
bsint64 = B.foldl (\a b -> 256 * a + cast b) 0 . bsPad 8

------

serialiseBlock :: Block -> ByteString
serialiseBlock b = prevHashBS (prevHash b)
                <> blockNumBS (blockNum b)
                <> versionNumBS (versionNum b)
                <> timestampBS (timestamp b)
                <> senderIDBS (senderID b)
                <> senderSignBS (senderSign b)
                <> recipientIDBS (recipientID b)
                <> transactionBS (transaction b)
                <> transactionInfoBS (transactionInfo b)
                <> bodyHashBS (bodyHash b)

prevHashBS :: Digest SHA512 -> ByteString
prevHashBS = dpack

blockNumBS :: Int64 -> ByteString
blockNumBS = encode

versionNumBS :: Int32 -> ByteString
versionNumBS = encode

timestampBS,senderIDBS,recipientIDBS :: Int64 -> ByteString
timestampBS = encode
senderIDBS = encode
recipientIDBS = encode

senderSignBS :: Maybe SB.ByteString -> ByteString
senderSignBS = B.take 512 . fromMaybe zeros . fmap fromStrict

transactionBS :: Amount -> ByteString
transactionBS = encode

transactionInfoBS :: String -> ByteString
transactionInfoBS = bsPad 1364 . encode

bodyHashBS :: Maybe (Digest SHA512) -> ByteString
bodyHashBS = B.take 64 . fromMaybe zeros . fmap dpack

bsPad :: Int64 -> ByteString -> ByteString
bsPad n bs = let m = B.length bs in B.drop (m-n) bs <> B.take (n-m) zeros
