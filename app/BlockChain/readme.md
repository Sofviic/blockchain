# Version
1

`00 00 00 01`

# TODO
1. verify block chain
    1. hashes \
    1. timestamps \
1. verify block
1. verify signatures \
1. refactor serialisation (either: use `binary` package OR create custom `Serial` class) 

## Bugs
1. Transaction Info offset incorrect (look at json)
1. timestamp is incorrect (probs only taking `ns` instead of `10^9*ns+s`) (look at json)

# Blocks

2048 bytes each `[0x000..0x7ff]`.

location | length | Info
---|---|---
`000 - 03f` | `64B`   | [`SHA512(Block(n-1))`](#sha512blockn-1)
`040 - 047` | `8B`    | [`block number`](#block-number)
`048 - 04b` | `4B`    | [`version number`](#version-number)
`04c - 053` | `8B`    | [`timestamp`](#timestamp)
`054 - 05b` | `8B`    | [`sender`](#sender)
`05c - 25b` | `512B`  | [`sender signature`](#recipient-signature)
`25c - 263` | `8B`    | [`recipient`](#recipient)
`264 - 26b` | `8B`    | [`transaction`](#transaction)
`26c - 7bf` | `1364B` | [`transaction info`](#transaction-info)
`7c0 - 7ff` | `64B`   | [`SHA512(BlockBody(n))`](#sha512blockbodyn)

## `SHA512(Block(n-1))`
SHA512 hash of the entire 512 bytes of the previous block.

## `block number`
Counter from 0(the genisis block), unique to each block.
(Can be thought of as distance to the genisis block)

## `version number`
Is `1`, ie `00 00 00 01`.

## `timestamp`
Unix timestamp, int64, in nanoseconds since epoch.

## `sender`
ID of the sender, should be unique to each wallet.

## `sender signature`
512 bits RSA signature of:
- `DATA` [`SHA512(Block(n-1))`](#sha512blockn-1)
- `DATA` [`block number`](#block-number)
- `DATA` [`version number`](#version-number)
- `DATA` [`timestamp`](#timestamp)
- `DATA` [`sender`](#sender)
- `zero` [`sender signature`](#recipient-signature)
- `DATA` [`recipient`](#recipient)
- `DATA` [`transaction`](#transaction)
- `DATA` [`transaction info`](#transaction-info)
- `zero` [`SHA512(BlockBody(n))`](#sha512blockbodyn)

## `recipient`
ID of the recipient, should be unique to each wallet.

## `transaction`
`8B` integer number (ie `int64`) representing the transfer amount in μ€.

## `transaction info`
`1364B` (approximately `1.33kiB`) for notes & info; may be ignored.

Interpretation (ie whether ascii, utf8, ...etc) is left up to the sender.

## `SHA512(BlockBody(n))`
SHA512 hash of:
- `DATA` [`SHA512(Block(n-1))`](#sha512blockn-1)
- `DATA` [`block number`](#block-number)
- `DATA` [`version number`](#version-number)
- `DATA` [`timestamp`](#timestamp)
- `DATA` [`sender`](#sender)
- `DATA` [`sender signature`](#recipient-signature)
- `DATA` [`recipient`](#recipient)
- `DATA` [`transaction`](#transaction)
- `DATA` [`transaction info`](#transaction-info)
- `zero` [`SHA512(BlockBody(n))`](#sha512blockbodyn)

