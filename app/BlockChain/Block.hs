module BlockChain.Block where

import qualified Data.ByteString.Lazy as B
import Data.Int(Int64)
import Crypto.Hash(Digest,SHA512(..),hashWith)
import System.Clock(getTime,getRes,nsec,Clock(Realtime))
import Profiles.Keys
import BlockChain.Serialise
import BlockChain.Utils
import BlockChain.Types
import Utils.IO
import Types

type UnixTime = Int64

createBlock' :: Digest SHA512 -> Int64 -> UnixTime -> UserID -> UserID -> Amount -> String -> Block
createBlock' pHash num time sender recipient amount info
    = Block pHash num version time sender Nothing recipient amount info Nothing

createNewBlockWithpHash :: Digest SHA512 -> Int64 -> UserID -> UserID -> Amount -> String -> IO Block
createNewBlockWithpHash ph pn sender recipient amount info
    = do
        time <- nsec <$> getTime Realtime
        tres <- nsec <$> getRes Realtime
        if tres > 1
            then warning $ "Not enough clock resolution: " <> show tres <> "ns > 1ns"
            else pure ()
        return $ createBlock' ph pn time sender recipient amount info

createNewBlock :: Maybe Block -> UserID -> UserID -> Amount -> String -> IO Block
createNewBlock Nothing = createNewBlockWithpHash dzero 0
createNewBlock (Just b) = case bodyHash b of
                            Nothing -> error "block isn't complete: missing hash"
                            Just ph -> createNewBlockWithpHash ph $ succ $ blockNum b

signBlock :: Username -> Block -> IO Block
signBlock name b = do 
                    sign <- signBSWithProfile name . B.toStrict . serialiseBlock . zeroNonSignFields $ b
                    return b{senderSign=Just sign}

signBlockMaybe :: Username -> Block -> IO (Maybe Block)
signBlockMaybe name b = do 
                    msign <- signBSWithProfileMaybe name . B.toStrict . serialiseBlock . zeroNonSignFields $ b
                    case msign of
                        Nothing -> return Nothing
                        Just sign -> return $ Just b{senderSign=Just sign}

finaliseBlock :: Block -> Block
finaliseBlock b = b{bodyHash=Just . hashWith SHA512 . B.toStrict . serialiseBlock . zeroNonHashFields $ b}
