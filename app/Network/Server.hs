{-# LANGUAGE OverloadedStrings #-}
module Network.Server where

import Data.Int(Int64,Int32)
import Data.ByteString(ByteString,fromStrict)
import qualified Data.ByteString.Lazy as BSL(ByteString)
import Control.Monad.IO.Class(MonadIO)
import Data.Maybe(fromMaybe)
import BlockChain.Types(Block)
import BlockChain.Serialise(bsint32)
import Network.Simple.TCP

type Datum = ByteString
data Packet = Ping Datum
            | Pong Datum

            | Status_VerReq
            | Status_VerRes Int
            | Status_TimeReq
            | Status_TimeRes Int64
            | Status_InfoReq Datum
            | Status_InfoRes Datum

            | Request_VerReq
            | Request_VerRes Int
            | Request_TimeReq
            | Request_TimeRes Int64
            | Request_BlocksReq BlocksReqAction
            | Request_BlocksRes BlockReqActionRes
            | Request_InfoReq Datum
            | Request_InfoRes Datum

            | Push_BlockReq Block
            | Push_BlockRes (Either APIErr APIAck)
            | Push_InfoReq Datum
            | Push_InfoRes Datum

data BlocksReqAction = BRAList
                     | BRABlock Int64
                     | BRABlocks Int64 Int64
data BlockReqActionRes = BRARList Int64 [(Int64,Int64)]
                       | BRARBlock (Either APIErr Block)
                       | BRARBlocks (Either APIErr [Block])
data APIErr = ErrMalformed
            | ErrNonexistant
            | ErrMalformedBlock
            | ErrCorruptedSignature
            | ErrCorruptedBlock
            | ErrUnableToAddBlock
data APIAck = AckID Char

startServer :: MonadIO m => m a
startServer = serve HostAny "7300" $ \(conSock, sockAddr) -> do
  putStrLn $ "Connection: " <> show conSock <> " <-> " <> show sockAddr
  return ()

processPacket :: MonadIO m => Socket -> m (Maybe Packet)
processPacket sock = do
  mver <- fmap strictm $ recv sock 4
  if fmap bsint32 mver /= Just 1
    then return Nothing
    else do
    mtyp <- fmap strictm $ recv sock 4
    case mtyp of
      Nothing -> return Nothing
      Just typ -> if notElem (bsint32 typ) cPackets
                  then return Nothing
                  else do
        mlen <- fmap (fromEnum . bsint32) . strictm <$> recv sock 4
        case mlen of
          Nothing -> return Nothing
          Just len -> case bsint32 typ of
            0x11 -> recv sock len >>= return . Just . Ping . copurembs
            0x21 -> return . Just $ Status_VerReq
            0x23 -> return . Just $ Status_TimeReq
            0x2e -> recv sock len >>= return . Just . Status_InfoReq . copurembs
            0x31 -> return . Just $ Request_VerReq
            0x33 -> return . Just $ Request_TimeReq
            0x35 -> recv sock len >>= return . Just . Request_BlocksReq . undefined . copurembs
            0x3e -> recv sock len >>= return . Just . Request_InfoReq . copurembs
            0x45 -> recv sock len >>= return . Just . Push_BlockReq . undefined . copurembs
            0x4e -> recv sock len >>= return . Just . Push_InfoReq . copurembs
            _ -> return Nothing

cPackets :: [Int32]
cPackets = [ 0x11
           , 0x21
           , 0x23
           , 0x2e
           , 0x31
           , 0x33
           , 0x35
           , 0x3e
           , 0x45
           , 0x4e
           ]

strictm :: Functor a => a ByteString -> a BSL.ByteString
strictm = fmap fromStrict

copurembs :: Maybe ByteString -> ByteString
copurembs = fromMaybe ""

