# Version
1

`00 00 00 01`

# TODO
1. implement api
   1. server
   1. client

## Bugs
1. .

# PACKETS

At least 12 bytes `[0x0..0xb]`.

location | length | Info
---|---|---
`  0 -   3` | `4B` | [Version Number](#version)
`  4 -   7` | `4B` | [`TYPE`](#type)
`  8 -   b` | `4B` | `n` (ie `|data|` ie length)
`  c - c+n` | `nB` | `data`

smallest possible packet:
`00 00 00 01 . 00 00 00 00 . 00 00 00 00`

example [ping packet](#ping-ping):
`00 00 00 01 . 00 00 00 11 . 00 00 00 07 . 00 01 01 02 03 05 08`

and response [pong packet](#ping-pong):
`00 00 00 01 . 00 00 00 12 . 00 00 00 07 . 00 01 01 02 03 05 08`

## `TYPE`

ID | type
---|---
`00` | [`ignore`](#ignore)
`1x` | [`ping`](#ping)
`2x` | [`status`](#status)
`3x` | [`request`](#request)
`4x` | [`push`](#push)

## `ignore`

ID | type
---|---
`00` | [`ignore`](#ignore)

Ignore this packet. Server should do nothing.

## `ping`

ID | type
---|---
`10` | [`ping-noresponse`](#ping-noresponse)
`11` | [`ping-ping`](#ping-ping)
`12` | [`ping-pong`](#ping-pong)

### `ping-noresponse`
Server should do nothing.

### `ping-ping`
Server should send a [`ping-pong`](#ping-pong) packet, echoing the `data` recieved.

### `ping-pong`
Server should do nothing.

## `status`

Status of server.

ID | type
---|---
`20` | [`status-noresponse`](#status-noresponse)
`21` | [`status-version-req`](#status-version-req)
`22` | [`status-version`](#status-version)
`23` | [`status-time-req`](#status-time-req)
`24` | [`status-time`](#status-time)
`2e` | [`status-info-req`](#status-info-req)
`2f` | [`status-info`](#status-info)

### `status-noresponse`
Server should do nothing.

### `status-version-req`
Server should send a [`status-version`](#status-version) packet, the `data` being a sequence of 4 bytes of supported versions 
(ie only `00 00 00 01`).

### `status-version`
Server should do nothing.

### `status-time-req`
Server should send a [`status-time`](#status-time) packet, the `data` should be 8 bytes of the current unix timestamp (in nano seconds).

### `status-time`
Server should do nothing.

### `status-info-req`
General purpose, specification is server-dependent.
Server should send a [`status-info`](#status-info) packet, 
- if `data` field is empty, then server-specification of this field should be sent.

### `status-info`
Server should do nothing.

## `request`

Request data/info of/on the blockchain.

ID | type
---|---
`30` | [`request-noresponse`](#request-noresponse)
`31` | [`request-version-req`](#request-version-req)
`32` | [`request-version`](#request-version)
`33` | [`request-time-req`](#request-time-req)
`34` | [`request-time`](#request-time)
`35` | [`request-blocks-req`](#request-blocks-req)
`36` | [`request-blocks`](#request-blocks)
`3e` | [`request-info-req`](#request-info-req)
`3f` | [`request-info`](#request-info)

### `request-noresponse`
Server should do nothing.

### `request-version-req`
Server should send a [`request-version`](#request-version) packet, the `data` being 4 bytes of the version of the blockchain.
(eg `00 00 00 01`)

### `request-version`
Server should do nothing.

### `request-time-req`
Server should send a [`request-time`](#request-time) packet, the `data` should be 8 bytes of the unix timestamp (in nano seconds) of the last block.

### `request-time`
Server should do nothing.

### `request-blocks-req`
Server should send a [`request-blocks`](#request-blocks) packet, 
- if `data` field is empty, then `list` action is taken.

`data`(in ascii) | length of rest of `data` | rest of `data` | response
---|---|---|---
`list ` | 0B | N/A | [`request-blocks:list`](#request-blocks-list)
`block ` | 8B | `block-number` | [`request-blocks:block`](#request-blocks-block)
`blocks ` | 16B | `block-number``block-number` | [`request-blocks:blocks`](#request-blocks-blocks)

#### `request-blocks:list`
8 bytes of the block number of the last block (call it `n`),
followed by a sequence of `n+1` 16-bytes of a pair of unix timestamps (in nano seconds) of all the blocks in the blockchain.
(each pair has the timestamp recorded in the block, and the timestamp of when the server recieved it)

#### `request-blocks:block`
Should contain exactly 8B representing the number of the block to be fetched.
Return `Block:` followed by 2kiB representing the block.
Case of errors:
- Not exactly 8B: `Error:Malformed`
- Block doesnt exist: `Error:Nonexistant`

#### `request-blocks:blocks`
Should contain exactly 16B representing the number of the min & the max (inclusive) to be fetched.
(clamped to the current available numbers in the blockchain)
Return `Block:` followed by 2kiB per block with block numbers in the interval min to max (inclusive & clamped).
Case of errors:
- Not exactly 16B: `Error:Malformed`

Note: To fetch the entire blockchain, one can write `00 00 00 00 . 00 00 00 00 : 11 11 11 11 . 11 11 11 11`)

### `request-blocks`
Server should do nothing.

### `request-info-req`
General purpose, specification is server-dependent.
Server should send a [`request-info`](#request-info) packet, 
- if `data` field is empty, then server-specification of this field should be sent.

### `request-info`
Server should do nothing.


## `push`

Send/push data/info onto the server/blockchain.

ID | type
---|---
`40` | [`push-noresponse`](#push-noresponse)
`45` | [`push-block`](#push-blocks)
`46` | [`push-block-res`](#push-blocks-res)
`4e` | [`push-info`](#push-info)
`4f` | [`push-info-res`](#push-info-res)

### `push-noresponse`
Server should do nothing.

### `push-block`
`data` should contain 2kiB representing the block.
Server should send a [`push-blocks-res`](#push-blocks-res) packet.
Case of errors:
- Not exactly 2kiB: `Error:Malformed`
- Block is malformed: `Error:MalformedBlock`
- Block failed key verification: `Error:CorruptedSignature`
- Block failed block verification: `Error:CorruptedBlock`
- Block failed blockchain verification: `Error:UnableToAddBlock`
- otherwise: `AckID:0`

### `push-block-res`
Server should do nothing.

### `push-info`
General purpose, specification is server-dependent.
Server should send a [`push-info-res`](#push-info-res) packet, 
- if `data` field is empty, then server-specification of this field should be sent.

### `push-info-res`
Server should do nothing.


