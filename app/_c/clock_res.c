#include "stdio.h"
#include "stdlib.h"
#include "time.h"

int main() {
    struct timespec ts;
    if (clock_getres(CLOCK_REALTIME, &ts) == -1) {
        perror("clock_getres");
        exit(EXIT_FAILURE);
    }
    printf("resolution: %lds %ldns\n", ts.tv_sec, ts.tv_nsec);
    return 0;
}
