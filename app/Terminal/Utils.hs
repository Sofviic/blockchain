module Terminal.Utils where

import Utils.Iso
import Data.Char

untilJust :: Monad m => m (Maybe a) -> m a
untilJust p = do
                ma <- p
                case ma of
                    Just a -> return a
                    Nothing -> untilJust p

amountValidator :: String -> Maybe ()
amountValidator = boolMaybe . all isDigit

alwaysValidator :: String -> Maybe ()
alwaysValidator = const $ return ()
