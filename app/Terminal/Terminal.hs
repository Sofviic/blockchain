module Terminal.Terminal where

import qualified Data.ByteString.Lazy as BL
import Data.Maybe(listToMaybe)
import Data.Char(toLower)
import Data.Aeson(encode)
import Text.Read(readMaybe)
import System.Clock(getRes,Clock(Realtime))
import Control.Monad(join)
import Profiles.Keys
import Profiles.Verify
import Profiles.Utils(getID,getContactID)
import BlockChain.BlockChain
import BlockChain.Types
import Utils.OldTypewriters(rmRS)
import Utils.FilePaths
import Utils.Enum
import Utils.Iso
import Terminal.Utils

prompt :: IO ()
prompt = putStr ">> "

timespec :: IO ()
timespec = getRes Realtime >>= putStrLn . show

hello :: IO ()
hello = readFile (sourceDir <> "hello") >>= putStrLn

help :: IO ()
help = readFile (sourceDir <> "help") >>= putStrLn

version :: IO ()
version = putStrLn "v1"

idk :: Maybe String -> IO ()
idk (Just s) = putStrLn $ "Sorry I couldn't parse that. All I parsed was \"" <> s <> "\"."
idk Nothing = putStrLn "Sorry I couldn't parse that. All I parsed was \"Nothing\"."

profilePrompt :: IO ()
profilePrompt = do
                putStr "Generate Profile: "
                name <- getAnswer
                if isValidProfileName name
                    then generateProfile name >> putStrLn "Profile Generated."
                    else putStrLn "Invalid Profile Name. ([A-Z]{4})"

genesisPrompt :: IO ()
genesisPrompt = do
                putStr "Generate New BlockChain (WILL OVERWRITE)? (y/N) "
                ans <- getAnswerC
                case ans of
                    Just 'y' -> createNewDirectory blockChainDir
                    _ -> putStrLn "Terminating."

deletionPrompt :: IO ()
deletionPrompt = do
                putStr "Delete BlockChain (UNREVERSIBLE)? (y/N) "
                ans <- getAnswerC
                case ans of
                    Just 'y' -> deleteDirectory blockChainDir
                    _ -> putStrLn "Terminating."

newTransactionPrompt :: IO ()
newTransactionPrompt = do
                        sender <- untilJust $ 
                            promptAndCheck "Profile: " "No such profile." getID
                        recipient <- untilJust $ 
                            promptAndCheck "To whom: " "No known contact with such a name." getContactID
                        amount <- fmap ((10^6 *) . read) . untilJust $ 
                            promptAndCheck "Amount(€): " "Enter a valid amount. ([0-9]*)" (return . amountValidator)
                        info <- untilJust $ 
                            promptAndCheck "Info/Description: " "Whoops; how did you break this?!" (return . alwaysValidator)
                        bc <- loadBlockChain blockChainDir
                        mbc <- createNewTransaction sender recipient amount info bc
                        case mbc of
                            Just nbc -> saveBlockChain blockChainDir nbc
                            _ -> putStrLn "Failed at adding a new transaction."

collapseDisplay :: IO ()
collapseDisplay = do
                    bc <- loadBlockChain blockChainDir
                    putStrLn "Net Totals:"
                    mapM_ (\(uid,amount) -> putStrLn $ "ID: " <> show uid <> ", Amount: " <> show (cast amount / 1000000)) $ collapse bc

blocksJSONPrompt :: IO ()
blocksJSONPrompt = do
                    putStrLn "Jsonifying blocks:-"
                    bc <- loadBlockChain blockChainDir
                    from <- untilJust $ 
                        promptAndCheckDef "0" "From [0]: " "Doesn't exist." (pure . join . fmap (boolMaybe . (`elem` [0..length bc - 1])) . readMaybe)
                    to <- untilJust $ 
                        promptAndCheckDef (show $ length bc - 1) ("To ["<>(show $ length bc - 1)<>"]: ") "Doesn't exist." (pure . join . fmap (boolMaybe . (`elem` [0..length bc - 1])) . readMaybe)
                    mapM_ (\b -> if blockNum b `elem` [read from..read to]
                                    then do
                                        BL.writeFile (blockChainDir <> show (blockNum b) <> ".block.full.json") $ encode b
                                        BL.writeFile (blockChainDir <> show (blockNum b) <> ".block.json")  $ encode $ minimiseBlock b
                                    else return ()) bc

promptAndCheck :: String -> String -> (String -> IO (Maybe a)) -> IO (Maybe String)
promptAndCheck pprompt err check = do
                                    putStr pprompt
                                    a <- getAnswer
                                    mid <- check a
                                    case mid of
                                        Nothing -> putStrLn err >> return Nothing
                                        Just _ -> return . Just $ a

promptAndCheckDef :: String -> String -> String -> (String -> IO (Maybe a)) -> IO (Maybe String)
promptAndCheckDef def pprompt err check = do
                                    putStr pprompt
                                    a <- getAnswer
                                    if a == "" 
                                        then return . Just $ def
                                        else do
                                            mid <- check a
                                            case mid of
                                                Nothing -> putStrLn err >> return Nothing
                                                Just _ -> return . Just $ a

getAnswer :: IO String
getAnswer = fmap rmRS getLine
getAnswerC :: IO (Maybe Char)
getAnswerC = fmap toLower . listToMaybe . rmws <$> getAnswer

rmws :: String -> String
rmws = dropWhile (`elem` " \t\n\r") . reverse . dropWhile (`elem` " \t\n\r") . reverse
