{-# LANGUAGE OverloadedStrings #-}
module Terminal.Testing where

import qualified Data.ByteString.Char8 as BC
import Crypto.Hash
import Crypto.PubKey.RSA
import Crypto.PubKey.RSA.PKCS15
import Utils.Hex
import Utils.Show
import Utils.OldTypewriters(rmR)

rsaPrompt :: IO ()
rsaPrompt = do
            (publicKey, privateKey) <- generate 512 0x10001
            writeFile "src/test_rsa" . show $ privateKey
            writeFile "src/test_rsa.pub" . show $ publicKey
            kpri <- readFile "src/test_rsa"
            kpub <- readFile "src/test_rsa.pub"
            BC.putStrLn . ("privateKey: "<>) . bshow $ privateKey == read kpri
            BC.putStrLn . ("publicKey : "<>) . bshow $ publicKey  == read kpub
            BC.putStr "Msg: "
            msg <- rmR <$> BC.getLine
            let digest = sign Nothing (Just SHA512) privateKey msg
            case digest of
                Left err -> BC.putStrLn . ("Error!: "<>) . bshow $ err
                Right s -> do
                    BC.putStr "signature: "
                    BC.putStrLn . hex $ s
                    BC.putStr "verified: "
                    BC.putStrLn . bshow $ verify (Just SHA512) publicKey msg s


hashPrompt :: IO ()
hashPrompt = do
            BC.putStr "Msg: "
            digest <- hashWith SHA512 . rmR <$> BC.getLine
            BC.putStrLn . bshow $ digest



