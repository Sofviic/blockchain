module Utils.Bases where

baseDigit :: Integral a => a -> a -> a -> a
baseDigit base 0 x = x `mod` base
baseDigit base i x = baseDigit base (i-1) $ x `div` base

baseDigits :: Integral a => a -> a -> [a]
baseDigits base x = fmap (\i -> baseDigit base i x) [0..]
