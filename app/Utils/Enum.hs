module Utils.Enum where

cast :: (Enum a, Enum b) => a -> b
cast = toEnum . fromEnum
