{-# LANGUAGE OverloadedStrings #-}
module Utils.OldTypewriters where

import Data.ByteString(ByteString)
import qualified Data.ByteString as B

rmR :: ByteString -> ByteString
rmR = B.filter (`B.notElem` "\r")

rmRS :: String -> String
rmRS = filter (/='\r')
