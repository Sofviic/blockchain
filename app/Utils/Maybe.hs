module Utils.Maybe where

rightToMaybe :: Either a b -> Maybe b
rightToMaybe (Left _) = Nothing
rightToMaybe (Right x) = Just x

forgetMaybe :: Maybe a -> Maybe ()
forgetMaybe = fmap $ const ()
