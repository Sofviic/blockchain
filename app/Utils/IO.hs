module Utils.IO where

warning :: String -> IO ()
warning = putStrLn . ("Warning: "<>)
