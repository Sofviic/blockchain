module Utils.Intify where

import Data.Int(Int64)
import Control.Monad(ap)
import Utils.Bases
import Utils.Enum

int64ify :: String -> Int64
int64ify = foldl (\b a -> 256 * b + (toEnum . fromEnum) a) 0 . take 8 . (<>repeat (toEnum 0))
unint64ify :: Int64 -> String
unint64ify = fmap cast . ap (fmap (baseDigit 256) $ reverse [0..7]) . pure
