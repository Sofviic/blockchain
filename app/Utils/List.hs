module Utils.List where

dropLast :: Int -> [a] -> [a]
dropLast n = reverse . drop n . reverse

getSeqFrom :: (Eq a, Enum a) => a -> [a] -> [a]
getSeqFrom s xs | s `notElem` xs = []
getSeqFrom s xs = s : getSeqFrom (succ s) xs

getSeqFromOn :: (Eq b, Enum b) => (a -> b) -> b -> [a] -> [a]
getSeqFromOn f s xs | s `notElem` (fmap f xs) = []
getSeqFromOn f s xs = head (getsResult f s xs) : getSeqFromOn f (succ s) xs

getsResult :: Eq b => (a -> b) -> b -> [a] -> [a]
getsResult _ _ [] = []
getsResult f b (x:xs) = (if f x == b then (x:) else id) (getsResult f b xs)

splice :: Int -> Int -> [a] -> [a]
splice offset len = take len . drop offset

onEveryLink :: (a -> a -> Bool) -> [a] -> Bool
onEveryLink p (a:b:r) = p a b && onEveryLink p (b:r)
onEveryLink _ _ = True
