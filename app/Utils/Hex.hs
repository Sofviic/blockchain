{-# LANGUAGE OverloadedStrings #-}
module Utils.Hex where

import Data.Word
import Data.ByteString(ByteString)
import qualified Data.ByteString as B

hex :: ByteString -> ByteString
hex = B.concatMap hexByte

hexByte :: Word8 -> ByteString
hexByte w = hexDigitMSB w <> hexDigitLSB w <> " "
    where
        hexDigitLSB = hexDigit . (`mod` 16)
        hexDigitMSB = hexDigit . (`div` 16)

hexDigit :: Word8 -> ByteString
hexDigit x | x `elem` [0..9] = B.singleton $ x + 0x30 -- + '0'
hexDigit x | x `elem` [10..15] = B.singleton $ x - 10 + 0x61 -- -10 + 'a'
hexDigit x = error $ "Error: x = " <> show x <> " is not a single hex digit."

unhex :: Enum a => Char -> a
unhex c | c `elem` ['0'..'9'] = toEnum $ fromEnum c - fromEnum '0'
unhex c | c `elem` ['a'..'f'] = toEnum . (+10) $ fromEnum c - fromEnum 'a'
unhex c = error $ "Error: x = " <> show c <> " is not a single hex digit."
