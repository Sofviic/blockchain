module Utils.FilePaths where

import System.Directory(removePathForcibly,createDirectoryIfMissing)

sourceDir :: FilePath
sourceDir = "src/"

profilesDir :: FilePath
profilesDir = sourceDir

contactsDir :: FilePath
contactsDir = sourceDir <> "contacts/"

blockChainDir :: FilePath
blockChainDir = sourceDir <> "blockchain/"

createNewDirectory,deleteDirectory :: FilePath -> IO ()
createNewDirectory = liftA2 (>>) removePathForcibly $ createDirectoryIfMissing True
deleteDirectory = removePathForcibly
