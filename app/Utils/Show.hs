module Utils.Show where

import Data.ByteString(ByteString)
import qualified Data.ByteString.Lazy as LB (ByteString,take,drop)
import qualified Data.ByteString.Char8 as BC
import qualified Data.ByteString.Lazy.Char8 as LBC
import Utils.Hex(unhex)
import Data.Int(Int64)

un0 :: String -> String
un0 = takeWhile (/= toEnum 0)

bshow :: Show a => a -> ByteString
bshow = BC.pack . show

lbsplice :: Int64 -> Int64 -> LB.ByteString -> LB.ByteString
lbsplice offset len = LB.take len . LB.drop offset

dpack :: Show a => a -> LB.ByteString
dpack = LBC.pack . base16ToBytes . show

spack :: Show a => a -> String
spack = base16ToBytes . show

base16ToBytes :: String -> String
base16ToBytes [] = []
base16ToBytes [x] = error "Error!: trying to convert base16 to base64; found hanging digit " <> show x
base16ToBytes (a:b:xs) = toEnum (16 * unhex a + unhex b) : base16ToBytes xs
