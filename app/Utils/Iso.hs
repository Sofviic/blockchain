module Utils.Iso where

boolMaybe :: Bool -> Maybe ()
boolMaybe True = Just ()
boolMaybe False = Nothing
maybeBool :: Maybe () -> Bool
maybeBool (Just ()) = True
maybeBool Nothing = False

-- class Iso a b where
--     to :: a -> b
--     from :: b -> a

